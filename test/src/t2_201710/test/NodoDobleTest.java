package t2_201710.test;

import junit.framework.TestCase;
import model.data_structures.NodoDoble;

public class NodoDobleTest extends TestCase
{

	private NodoDoble<String> nodo1;
	private NodoDoble<String> nodo2;
	private NodoDoble<String> nodo3;
	private NodoDoble<String> nodo4;
	private NodoDoble<String> nodo5;
	
	private void setupEscenario1(){
		nodo1 = new NodoDoble<String>();
		nodo2 = new NodoDoble<String>();
	}
	
	private void setupEscenario2(){
		nodo1 = new NodoDoble<String>(null,null,null);
		nodo2 = new NodoDoble<String>("Machete",nodo3,null);
		nodo3 = new NodoDoble<String>("El lugar donde todo termina",nodo4,nodo2);
		nodo4 = new NodoDoble<String>("Las tortugas tambi�n pueden volar",null,nodo3);
		nodo5 = new NodoDoble<String>("Titanic 2", null, nodo4 );
		nodo1 = new NodoDoble<String>(null,null,null);
		nodo2 = new NodoDoble<String>("Machete",nodo3,null);
		nodo3 = new NodoDoble<String>("El lugar donde todo termina",nodo4,nodo2);
		nodo4 = new NodoDoble<String>("Las tortugas tambi�n pueden volar",null,nodo3);
		
		
		
		
	}
	
	public void testNodoDobleVacio(){
		setupEscenario1();
		assertNull("No deber�a tener elemento", nodo1.darElem());
		assertNull("No deber�a tener anterior", nodo1.darAnterior());
		assertNull("No deberia tener siguiente", nodo1.darSiguiente());

		nodo1.establecerElem("La la land");
		nodo1.establecerSiguiente(nodo2);

		assertNotNull("Deber�a tener el elemento 'La la land'", nodo1.darElem());
		assertNotNull("Deber�a tener como siguiente el nodo2", nodo1.darSiguiente());
	}
	
	public void testNodoDobleLleno(){
		setupEscenario2();
		
		assertNotNull("Deber�a tener siguiente", nodo3.darSiguiente());
		assertNull("No deber�a tener siguiente", nodo5.darSiguiente());
		assertNotNull("Deberia tener anterior", nodo3.darAnterior());
		assertNotNull("Deberia tener anterior", nodo5.darAnterior());
		assertNotNull("Deber�a tener item", nodo2.darElem());
		
		assertNull("No deber�a tener item", nodo1.darElem());
		
		nodo1.establecerElem("Belleza inesperada");
		assertEquals("Deber�a tener item","Belleza inesperada" , nodo1.darElem());
		
		assertNull("No deber�a tener siguiente", nodo4.darSiguiente());
		
		nodo4.establecerSiguiente(nodo5);
		assertNotNull("Deber�a tener siguiente", nodo4.darSiguiente());
		
	}
}
