package t2_201710.test;

import junit.framework.TestCase;
import model.data_structures.NodoSencillo;

public class NodoSencilloTest extends TestCase{

	// Atributos

	/**
	 * Clase en la cual se hacen las pruebas
	 */
	private NodoSencillo<String> nodo1;
	private NodoSencillo<String> nodo2;
	private NodoSencillo<String> nodo3;
	private NodoSencillo<String> nodo4;
	private NodoSencillo<String> nodo5;

	//Escenarios

	/**
	 * Escenario 1: Se crean dos nodos vac�os de tipo String
	 */

	private void setupEscenario1(){
		nodo1 = new NodoSencillo<String>();
		nodo2 = new NodoSencillo<String>();
	}

	/**
	 * Escenario 2: Se crean varios nodos de tipo String conectados entre ellos
	 */
	private void setupEscenario2(){
		nodo5 = new NodoSencillo<String>("Titanic 2", null);
		nodo4 = new NodoSencillo<String>("Las tortugas tambi�n pueden volar",null);
		nodo3 = new NodoSencillo<String>("El lugar donde todo termina",nodo4);
		nodo2 = new NodoSencillo<String>("Machete",nodo3);
		nodo1 = new NodoSencillo<String>(null,nodo2);

	}

	public void testNodoSencilloVacio(){
		setupEscenario1();
		assertNull("No deber�a tener item", nodo1.darItem());
		assertNull("No deber�a tener siguiente", nodo1.darSiguiente());

		nodo1.establecerItem("La la land");
		nodo1.establecerSiguiente(nodo2);

		assertNotNull("Deber�a tener el item 'La la land'", nodo1.darItem());
		assertNotNull("Debr�a tener como siguiente el nodo2", nodo1.darSiguiente());
	}

	public void testNodoSencilloLleno(){
		setupEscenario2();
		
		assertNotNull("Deber�a tener siguiente", nodo3.darSiguiente());
		assertNull("No deber�a tener siguiente", nodo5.darSiguiente());
		
		assertNotNull("Deber�a tener item", nodo2.darItem());
		
		assertNull("No deber�a tener item", nodo1.darItem());
		
		nodo1.establecerItem("Belleza inesperada");
		assertNotNull("Deber�a tener item", nodo1.darItem());
		
		assertNull("No deber�a tener siguiente", nodo4.darSiguiente());
		
		nodo4.establecerSiguiente(nodo5);
		assertNotNull("Deber�a tener siguiente", nodo4.darSiguiente());
		
	}


}
