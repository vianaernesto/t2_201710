package t2_201710.test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSencillo;

public class ListaEncadenadaTest  extends TestCase{

	private ListaEncadenada<String> lista;

	private NodoSencillo<String> nodo1;
	private NodoSencillo<String> nodo2;
	private NodoSencillo<String> nodo3;
	private NodoSencillo<String> nodo4;
	private NodoSencillo<String> nodo5;

	/**
	 * Escenario 1: La lista est� vac�a
	 */
	private void setupEscenario1(){
		lista = new ListaEncadenada<String>();
	}

	/**
	 * Escenario 2: La lista est� inicializada
	 */
	private void agregarNodos(){

		nodo5 = new NodoSencillo<String>();
		nodo4 = new NodoSencillo<String>();
		nodo3 = new NodoSencillo<String>();
		nodo2 = new NodoSencillo<String>();
		nodo1 = new NodoSencillo<String>();


		lista.agregarElementoFinal("Titanic 2");
		lista.agregarElementoFinal("Belleza inesperada");
		lista.agregarElementoFinal("La chica danesa");
		lista.agregarElementoFinal("Preciosa");
		lista.agregarElementoFinal("La duff");

	}

	public void testListaEncadenada(){
		setupEscenario1();

		//En este fragmento del c�digo se revisa que la lista sehaya creado vac�a
		assertEquals("El tama�o de la lista deber�a ser 0",0,lista.darNumeroElementos());
		assertEquals("Esta posici�n no deber�a tener ning�n elemento asignado ",null, lista.darElemento(0));
		assertEquals("No deberia ir a la posici�n anterior",false, lista.retrocederPosicionAnterior());
		assertEquals("No deberia ir a la siguiente posici�n",false,lista.avanzarSiguientePosicion());
		
		
		assertEquals("No deber�a tener siguiente",false,lista.iterator().hasNext());

		/*
			Se llena la lista con nodos de tipo Strign con el 
			fin de verificar que los metodos est�n funcionando correctamente
		 */
		agregarNodos();

		assertEquals("La longitud de la lista deber�a ser de 5",5,lista.darNumeroElementos());
		
		assertEquals("deber�a tener siguiente",true, lista.iterator().hasNext());
		assertEquals("El elemento deber�a ser 'Titanic 2' ", "Titanic 2", lista.darElementoPosicionActual());
		assertEquals("El elemento deber�a ser 'La chica danesa' ", "La chica danesa", lista.darElemento(3));

		//Se avanza y retrocede en las posiciones para saber 
		//si funcionan esos mentodos
		lista.avanzarSiguientePosicion();
		assertEquals("El elemento deber�a ser 'Preciosa'","Preciosa",lista.darElementoPosicionActual());

		lista.retrocederPosicionAnterior();
		assertEquals("El elemento deber�a ser 'La chica danesa'","La chica danesa", lista.darElementoPosicionActual());

	}



}
