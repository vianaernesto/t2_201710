package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoDoble;
import model.data_structures.NodoSencillo;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.csvreader.*;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	public static final String SEPARADOR =",";

	public static final String SEPARADOR2 = "[|]";

	public static final String PARENTESIS1 = "[(]";

	public static final String PARENTESIS2 = "[)]";


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		misPeliculas = new ListaDobleEncadenada<VOPelicula>(); 
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

		try{
			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));

			String line = br.readLine();

			while(line!= null ){

				VOPelicula temp = new VOPelicula();
				String data = line;
				temp.setAgnoPublicacion(sacarAgno(data));
				String[] dataSplit = data.split(","); 
				temp.setTitulo(sacarNombre(dataSplit));
				temp.setGenerosAsociados(sacarGeneros(dataSplit));
				misPeliculas.agregarElementoFinal(temp);
				line = br.readLine();
			}

			br.close();

		}
		catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("Cargando...");
		guardarPorAgno();
		System.out.println("Listo...");
	}

	public void guardarPorAgno(){
		ListaDobleEncadenada<VOPelicula> lista = new ListaDobleEncadenada<VOPelicula>();
		lista = (ListaDobleEncadenada<VOPelicula>) misPeliculas;

		sort(lista);
		lista.cambiarActualPrimero();
		int agno = -1;

		ListaDobleEncadenada<VOPelicula> listaPeli = null;

		VOAgnoPelicula tempo = null;

		VOPelicula voP = null;
		for(int i =0; i < lista.darNumeroElementos();i++){

			voP =lista.darActual().darElem();

			if(agno != voP.getAgnoPublicacion()){

				if(listaPeli != null){

					tempo.setPeliculas(listaPeli);
					peliculasAgno.agregarElementoFinal(tempo);
				}

				listaPeli = new ListaDobleEncadenada<VOPelicula>();
				tempo = new VOAgnoPelicula();
				agno = voP.getAgnoPublicacion();
				tempo.setAgno(agno);

			}
			listaPeli.agregarElementoFinal(voP);
			lista.avanzarSiguientePosicion();
		}

	}


	public void sort(ListaDobleEncadenada<VOPelicula> lista){
		shuffle(lista);
		sort(lista,0, lista.darNumeroElementos());
	}

	private void sort(ListaDobleEncadenada<VOPelicula> lista,int lo, int hi){
		if(hi<=lo)return;
		int j= partition(lista, lo, hi);
		sort(lista,lo,j-1);
		sort(lista, j+1, hi);
	}

	private int partition(ListaDobleEncadenada<VOPelicula> lista, int lo, int hi){

		int i = lo, j = hi+1;

		while(true){
			while(less(lista.darElemento(++i), lista.darElemento(lo)))
				if(i == hi)break;

			while(less(lista.darElemento(lo),lista.darElemento(--j)))
				if(j==lo)break;

			if(i >= j) break;
			exch(lista,i,j);
		}

		exch(lista,lo,j);
		return j;
	}

	private boolean less(VOPelicula v , VOPelicula w){
		if(v.getAgnoPublicacion() <= w.getAgnoPublicacion()){return true;}else{return false;}
	}

	private void exch(ListaDobleEncadenada<VOPelicula> lista,int i, int j ){

		NodoDoble<VOPelicula> posI =lista.darNodoPosicion(i);
		NodoDoble<VOPelicula> posJ = lista.darNodoPosicion(j);

		VOPelicula item1 = posI.darElem();
		posI.establecerElem(posJ.darElem());
		posJ.establecerElem(item1);
	}


	public  void shuffle(ListaDobleEncadenada<VOPelicula> lista){

		Random rdm = new Random();

		NodoDoble<VOPelicula> temp1 =null;
		NodoDoble<VOPelicula> temp2 = null;

		VOPelicula item2 = null;
		int n = lista.darNumeroElementos();
		for(int i =0; i < lista.darNumeroElementos(); i++){

			int pos = rdm.nextInt(n);
			int pos2 = rdm.nextInt(n);

			temp1 = lista.darNodoPosicion(pos);
			temp2 = lista.darNodoPosicion(pos2);

			item2 = temp2.darElem();

			temp2.establecerElem(temp1.darElem());
			temp1.establecerElem(item2);


		}

	}

	public ILista<String> sacarGeneros(String[] dataSplit){

		ILista<String> generos = new ListaEncadenada<String>();

		String[] generosSplit = dataSplit[dataSplit.length-1].split("[|]");
		for(String tempo: generosSplit){
			generos.agregarElementoFinal(tempo);
		}

		return generos;
	}

	public String sacarNombre(String[] dataSplit){
		String nombre1 = "";
		String nombre = "";
		for(int i = 1; i < dataSplit.length-1; i++){

			nombre1 =  dataSplit[i];
			nombre += nombre1.replaceAll("\"", "");
		}

		return nombre;
	}

	public int sacarAgno(String data){

		String[] arregloAgno = data.split("[(]");
		int agno =0;

		for(int i =1; i < arregloAgno.length && agno ==0; i++){

			String temp = arregloAgno[arregloAgno.length-i].split("[)]")[0];
			if(isNumber(temp))
				agno = Integer.parseInt(temp.split("-")[0]);
		}


		return agno;

	}

	public boolean isNumber(String dato){
		try{
			Double d = Double.parseDouble(dato);
		}
		catch(NumberFormatException e){
			String agno= dato.split("-")[0];
			try{
				Double dou = Double.parseDouble(agno);

			}
			catch(NumberFormatException e2)
			{
				return false;
			}
			return true;
		}
		return true;
	}


	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ILista<VOPelicula> peliculasSubcadena = new ListaEncadenada<VOPelicula>();

		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
		{
			String buscado = misPeliculas.darElemento(i).getTitulo();

			if(buscado.indexOf(busqueda) != -1)
			{
				peliculasSubcadena.agregarElementoFinal(misPeliculas.darElemento(i));
			}


		}


		return peliculasSubcadena;

	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {

		ILista<VOPelicula> listaAgno = new ListaEncadenada<VOPelicula>();

		for(int i =0; i < misPeliculas.darNumeroElementos(); i++)
		{
			int anio = misPeliculas.darElemento(i).getAgnoPublicacion();

			if(anio == agno)
			{
				listaAgno.agregarElementoFinal(misPeliculas.darElemento(i));
			}
		}

		return listaAgno;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {

		if(peliculasAgno.avanzarSiguientePosicion()!= false)
		{
			return peliculasAgno.darElementoPosicionActual();
		}
		else
			return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {

		if(peliculasAgno.retrocederPosicionAnterior()!= false)
		{
			return peliculasAgno.darElementoPosicionActual();
		}
		else
			return peliculasAgno.darElementoPosicionActual();
	}

}
